//#version 120
uniform sampler2D texture0;
uniform vec3 lightpos;
uniform vec3 viewpos;

varying vec3 normal;
varying vec4 color;
varying vec2 texcoord;
varying vec3 pos; 

void main (void){
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * vec3(1.0, 1.0, 1.0);

    vec3 light = lightpos - pos;
    vec3 diffuse = 0.8 * max(dot(normalize(normal), normalize(light)),0.0) * vec3(1.0, 1.0, 1.0);
    
    // viewdir (V) and reflectdir (R)
    vec3 viewdir = normalize(viewpos-pos);
    vec3 reflectdir = reflect(-normalize(light), normalize(normal));
    // specular term, n = 10.0
    float spec = pow(max(dot(viewdir, reflectdir), 0.0), 10.0);
    vec3 specular = 2.5 * spec * vec3(1.0, 1.0, 1.0);

    gl_FragColor = vec4((diffuse + ambient + specular) * color.xyz,1.0);
}
