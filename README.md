# Icosahedron
19M38254 - Roland Hartanto

## How to Run
### Requirements
Copy lib folder that contains the jar(s) of: (the same as given libraries in `simpleexampleico2019.tar.gz`)
- Gluegen
- Jogl
- jama.jar

to this folder. The structure should be like this:
```
SimpleExample2019
|- lib
|  |- jar
|- resource
|- src
|- Makefile
|- README.md

```

### Running The Program
1. Run this command in this folder (SimpleExampleIco2019)
```
make clean
make
```
2. The animation window should be opened after running the commands in number 1.
